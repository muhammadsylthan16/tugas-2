package pemlan.Tugas2;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();
        boolean next = true;
        
        while (next) {
            System.out.print("Masukkan Nama Mahasiswa: ");
            String nama = sc.nextLine();
            System.out.print("Masukkan NIM Mahasiswa: ");
            String nim = sc.nextLine();
            System.out.print("Masukkan Alamat Mahasiswa: ");
            String alamat = sc.nextLine();

            Mahasiswa mahasiswa = new Mahasiswa(nama, nim,alamat);
            mahasiswaList.add(mahasiswa);

            System.out.print("Tambahkan Data Mahasiswa Lagi (y/n)? ");
            String jawab = sc.nextLine();
            if (jawab.equalsIgnoreCase("n")) {
                next = false;
            } else if (jawab.equalsIgnoreCase("y")){
                next = true;
            } else {
                System.out.println("Perintah Tidak Valid");
                next = false;
            }
        }

        System.out.println("\n=======================================================================");
        System.out.printf("|%-25s | %-17s | %-20s | \n", "Nama", "NIM" , "Alamat");
        System.out.println("=======================================================================");
        for (Mahasiswa mahasiswa : mahasiswaList) {
            System.out.printf("|%-25s | %-17s | %-20s | \n", mahasiswa.getNama(), mahasiswa.getNim(),mahasiswa.getAlamat());
        }
        System.out.println("=====================================================================");

        sc.close();
    }
}
   
